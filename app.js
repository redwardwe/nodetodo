const path=require('path');
const express=require('express');
const app= express();
const bodyPrser=require('body-parser');
const rutaMain=require('./routes/root.js');

//cadavez ke pase por el bodyparser
app.use(bodyPrser.urlencoded({extended: false}));
//importamos Public par ke no de fallo los css
app.use(express.static(path.join(__dirname,'public')));
/**
 * Demas secciones de mi api
 * 
 * 

 * 
 * 

 */

app.use(rutaMain);
//config 404+ init
app.use((req, res, next)=> {
    res.status(404).end();
});
app.listen(3000, ()=>{console.log("ServerOn!!! ")});